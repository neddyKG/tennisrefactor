var TennisGame2 = function(player1Name, player2Name) {
    this.P1point = 0;
    this.P2point = 0;
    this.score = "";

    this.P1res = "";
    this.P2res = "";

    this.player1Name = player1Name;
    this.player2Name = player2Name;
};

TennisGame2.prototype.getLiteralScore = function() {
    
    this.getLiteralScoreForFirst3Points(this.score);

    this.getTiedScoresGraterThan2();

    
    this.playerXwinningInFirst3Points(this.P1point, this.P2point);


    this.getPlayerAdvantage(this.P1point, this.P2point, 1);

    this.getPlayerAdvantage(this.P2point, this.P1point, 2);


    this.getPlayerWinner(this.P1point, this.P2point, 1);

    this.getPlayerWinner(this.P2point, this.P1point, 2);

    return this.score;
};


TennisGame2.prototype.getLiteralScoreForFirst3Points = function(score) {
    if (this.P1point === this.P2point && this.P1point < 3) {
        this.score = this.getLiteralPoint(this.P1point);
        this.score += "-All";
    }
}

TennisGame2.prototype.getTiedScoresGraterThan2 = function() {
    if (this.P1point === this.P2point && this.P1point > 2)
    this.score = "Deuce";
}


TennisGame2.prototype.playerXwinningInFirst3Points = function(P1point, P2point) {
    if (P1point > P2point && P1point < 4) {
        this.getLiteralScoreForDiffrentPoints(P1point, P2point);
    }

    if (this.P2point > this.P1point && this.P2point < 4) {
        this.getLiteralScoreForDiffrentPoints(P1point, P2point);
    }

}


TennisGame2.prototype.getLiteralScoreForDiffrentPoints = function(P1point, P2point) {
    this.P1res = this.getLiteralPoint(P1point);
    this.P2res = this.getLiteralPoint(P2point);
   
    this.score = this.P1res + "-" + this.P2res;
}


TennisGame2.prototype.getPlayerAdvantage = function(P1point, P2point, playerNumberAdvantage) {
    if (P1point > P2point && P2point >= 3) {
        this.score = "Advantage player"+playerNumberAdvantage;
    }
}


TennisGame2.prototype.getPlayerWinner = function(P1point, P2point, playerNumberWinner) {
if (P1point >= 4 && P2point >= 0 && (P1point - P2point) >= 2) {
    this.score = "Win for player"+playerNumberWinner;
}

}


TennisGame2.prototype.getLiteralPoint = function(point){
    if (point === 0)
        return "Love";
    if (point === 1)
        return "Fifteen";
    if (point === 2)
        return "Thirty";
    if (point === 3)
        return "Forty";
}


TennisGame2.prototype.SetP1Score = function(number) {
    var i;
    for (i = 0; i < number; i++) {
        this.P1Score();
    }
};

TennisGame2.prototype.SetP2Score = function(number) {
    var i;
    for (i = 0; i < number; i++) {
        this.P2Score();
    }
};

TennisGame2.prototype.P1Score = function() {
    this.P1point++;
};

TennisGame2.prototype.P2Score = function() {
    this.P2point++;
};

TennisGame2.prototype.wonPoint = function(player) {
    if (player === "player1")
        this.P1Score();
    else
        this.P2Score();
};

if (typeof window === "undefined") {
    module.exports = TennisGame2;
}